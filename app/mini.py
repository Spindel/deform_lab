"""Self-contained Deform demo example."""
from __future__ import print_function

# Pyramid
import colander
from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPFound
from pyramid.session import SignedCookieSessionFactory

# Deform
import deform


class ExampleSchema(deform.schema.CSRFSchema):

    name = colander.SchemaNode(colander.String(), title="Name")

    age = colander.SchemaNode(
        colander.Int(),
        default=18,
        title="Age",
        description="Your age in years",
    )

    gender = colander.SchemaNode(
        colander.String(),
        default="",
        title="Gender",
        widget=deform.widget.SelectWidget(values=[("has-pockets", "pockets"), ("no-pockets", "no pockets")]),
        description="Pick the gender of your choice",
    )

latest = {}


def mini_example_get(request):
    """Sample Deform form with validation."""
    global latest

    schema = ExampleSchema().bind(request=request)

    # Create a styled button with some extra Bootstrap 3 CSS classes
    process_btn = deform.form.Button(name="process", title="Process")
    form = deform.form.Form(schema, buttons=(process_btn,))
    # Render a form with initial default values
    rendered_form = form.render()

    return {
        # This is just rendered HTML in a string
        # and can be embedded in any template language
        "rendered_form": rendered_form,
        "result": latest,
    }



def mini_example_post(request):
    """Sample Deform form with validation."""
    global latest
    latest = {}

    if "process" not in request.params:
        return HTTPFound("/")

    schema = ExampleSchema().bind(request=request)
    # Create a styled button with some extra Bootstrap 3 CSS classes
    process_btn = deform.form.Button(name="process", title="Process")
    form = deform.form.Form(schema, buttons=(process_btn,))


    try:
        appstruct = form.validate(request.POST.items())
    except deform.exception.ValidationFailure as exc:
        return {
            "rendered_form": exc.render(),
            "result": latest,
        }
    # Thank user and take him/her to the next page
    request.session.flash("Thank you for the submission.")

    # Do Something with our validated data
    latest = appstruct
    # Redirect to the page shows after succesful form submission
    return HTTPFound("/")




def main(global_config, **settings):
    """pserve entry point"""
    session_factory = SignedCookieSessionFactory("seekrit!")
    config = Configurator(settings=settings, session_factory=session_factory)
    config.include("pyramid_chameleon")
    deform.renderer.configure_zpt_renderer()
    config.add_static_view("static_deform", "deform:static")
    config.add_route("mini_example", path="/")
    config.add_view(
        mini_example_post, route_name="mini_example", renderer="templates/mini.pt",
        request_method="POST",
    )
    config.add_view(
        mini_example_get, route_name="mini_example",
        renderer="templates/mini.pt", request_method="GET",
    )
    return config.make_wsgi_app()
