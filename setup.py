import sys

from setuptools import setup, find_packages

requires = [
    "pyramid",
    "deform",
    "colander>=1.7.0",
    "iso8601==0.1.11",
    "colander",
    "waitress",
    "pyramid_chameleon",
]

setup(
        name="app",
        version="0.0",
        classifiers=[],
        zip_safe=True,
        packages=find_packages(),
        install_requires=requires,
        entry_points={
            "paste.app_factory": ["mini= app.mini:main"],
        }
)
